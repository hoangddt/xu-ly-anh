import cv2, os
import numpy as np
from skimage import feature
from sklearn.svm import LinearSVC

from localbinarypattern import LocalBinaryPatterns

DEBUG = True
# int radius=1, int neighbors=8, int grid_x=8, int grid_y=8
# For face detection we will use the Haar Cascade provided by OpenCV.
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
# Todo: refactor
names = {}

# For face recognition we will the the LBPH Face Recognizer 
recognizer = LinearSVC(C=100.0, random_state=42)
LHB_descriptor = LocalBinaryPatterns(8, 1)

def detect_faces(gray_image):
    # this also detect object not face
    faces = faceCascade.detectMultiScale(
        gray_image,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags =  cv2.CASCADE_SCALE_IMAGE
    )
    result = []
    for (x, y, w, h) in faces:
        has_eye = False
        roi_gray = gray_image[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)

        if len(eyes):
            has_eye = True

        if has_eye:
            element = [x, y, w, h]
            result.append(element)
    return result

def load_images_and_labels(path):
    # each subdirectory in path contain a set of images for that person
    # name of subdirectory indicate name of person inside
    subdirectories = []
    # labels contain integer number of each person
    images = []
    labels = []
    # names contain pair of number and name
    global names

    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path, name)):
            subdirectories.append(name)

    current_label = 0
    for directory in subdirectories:
        current_dir = os.path.join(path, directory)
        image_paths = [os.path.join(current_dir, f) for f in os.listdir(current_dir)]

        for image_path in image_paths:
            # Read the image and convert to grayscale
            gray_img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
            faces = detect_faces(gray_img)
            # If face is detected, append the face to images and the label to labels
            for (x, y, w, h) in faces:
                images.append(gray_img[y: y + h, x: x + w])
                labels.append(current_label)
                names[current_label] = directory
                if DEBUG:
                    cv2.imshow("Adding faces to traning set...", gray_img[y: y + h, x: x + w])
                    cv2.waitKey(150)

        current_label = current_label + 1
    print "Label and names:", names
    return images, labels, names

def train_recognizer(path):
    images, labels, names = load_images_and_labels(path)
    names_train = [value for k, value in names.items()]
    # extract hist from images
    data = []
    for image in images:
        hist = LHB_descriptor.describe(image)
        print hist
        data.append(hist)
    recognizer.fit(data, names_train)
    # recognizer.setLabelsInfo(names)

def recognize_face_from_webcam(path):
    train_recognizer(path)
    webcam = cv2.VideoCapture(0)

    while True:
        return_value, frame = webcam.read()
        # turn the image captured to gray
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = detect_faces(gray_frame)
        for (x, y, w, h) in faces:
            face = gray_frame[y: y + h, x: x + w]
            hist = LHB_descriptor.describe(face)
            prediction = recognizer.predict(hist)[0]

            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            cv2.putText(frame, prediction, (x, y - 10), 4, 1.0, (0, 255, 0), 2)
            cv2.imshow("Video", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    webcam.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    # ToDo: refactor
    path = "./images"
    # recognize_face_from_webcam(path)
