import cv2, os
import numpy as np

DEBUG = False

# For face detection we will use the Haar Cascade provided by OpenCV.
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
# Todo: refactor
names = {}

# For face recognition we will the the LBPH Face Recognizer 
recognizer = cv2.createLBPHFaceRecognizer()

def detect_faces(gray_image):
    # this also detect object not face
    faces = faceCascade.detectMultiScale(
        gray_image,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags =  cv2.CASCADE_SCALE_IMAGE
    )
    result = []
    for (x, y, w, h) in faces:
        has_eye = False
        roi_gray = gray_image[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)

        if len(eyes):
            has_eye = True

        if has_eye:
            element = [x, y, w, h]
            result.append(element)
    return result

def load_images_and_labels(path):
    # each subdirectory in path contain a set of images for that person
    # name of subdirectory indicate name of person inside
    subdirectories = []
    # labels contain integer number of each person
    images = []
    labels = []
    # names contain pair of number and name
    global names

    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path, name)):
            subdirectories.append(name)

    current_label = 0
    for directory in subdirectories:
        current_dir = os.path.join(path, directory)
        image_paths = [os.path.join(current_dir, f) for f in os.listdir(current_dir)]

        for image_path in image_paths:
            # Read the image and convert to grayscale
            gray_img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
            faces = detect_faces(gray_img)
            # If face is detected, append the face to images and the label to labels
            for (x, y, w, h) in faces:
                images.append(gray_img[y: y + h, x: x + w])
                labels.append(current_label)
                names[current_label] = directory
                if DEBUG:
                    cv2.imshow("Adding faces to traning set...", gray_img[y: y + h, x: x + w])
                    cv2.waitKey(150)

        current_label = current_label + 1
    print "Label and names:", names
    return images, labels, names

def load_names(path):
    global names
    current_label = 0
    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path, name)):
            names[current_label] = name
            current_label = current_label + 1

def train_recognizer(path, trained_data_path):
    images, labels, names = load_images_and_labels(path)
    recognizer.train(images, np.array(labels))
    recognizer.save(trained_data_path)
    # recognizer.setLabelsInfo(names)

def load_trained_model(trained_data):
    recognizer.load(trained_data)

def recognize_face_from_webcam(path):
    webcam = cv2.VideoCapture(0)

    while True:
        return_value, frame = webcam.read()
        cv2.imshow("Video", frame)
        # turn the image captured to gray
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = detect_faces(gray_frame)
        if faces:
            for (x, y, w, h) in faces:
                # confidence = 0 is 100% accurate
                label_predicted, confidence = recognizer.predict(gray_frame[y: y + h, x: x + w])
                confidence = round(confidence)
                text = names[label_predicted] + "_ distance: " + str(confidence)
                # name = recognizer.getLabelInfo(label_predicted)
                # draw a rectangle around face and label+confidence
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                cv2.putText(frame, text, (x, y - 10), 4, 1.0, (0, 255, 0), 2)
                cv2.imshow("Video", frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        else:
            print "No face {0}".format(frame)
            # cv2.imshow("Video", frame)
    webcam.release()
    cv2.destroyAllWindows()

def recognize_from_test_data(path):
    file = open('output/report.txt', 'w')
    file.write("Testing report for face recognizer\n\n")
    subdirectories = []
    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path, name)):
            subdirectories.append(name)

    for directory in subdirectories:
        file.write("\nTesting on {0}:\n".format(directory))
        current_dir = os.path.join(path, directory)
        image_paths = [os.path.join(current_dir, f) for f in os.listdir(current_dir)]
        realname = directory
        for image_path in image_paths:
            # Read the image and convert to grayscale
            gray_img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
            faces = detect_faces(gray_img)
            # If face is detected, append the face to images and the label to labels
            for (x, y, w, h) in faces:
                # confidence = 0 is 100% accurate
                label_predicted, confidence = recognizer.predict(gray_img[y: y + h, x: x + w])
                confidence = round(confidence)
                name_predicted = names[label_predicted]
                correct = True if name_predicted == realname else False
                file.write("    {} : predict: {} - distance: {}\n".format(correct, name_predicted, confidence))
                print "    {} : predict: {} - distance: {}\n".format(correct, name_predicted, confidence)

    file.close()

if __name__ == "__main__":
    # ToDo: refactor
    path = "./images"
    img_test_path = './test_images/TestData'
    trained_data_path = './output/data.xml'

    print "Do you want to load data or re-train model? (l/t)"
    opt = raw_input()
    if opt == 'l':
        print "Start loading names..."
        # images, labels, names = load_images_and_labels(path)
        # load_names(path)
        names = {0: 'Lam', 1: 'Nghia', 2: 'Thien', 3: 'Hao', 4: 'Hieu', 5: 'Hoang', 6: 'Loi', 7: 'Hoa', 8: 'Kayvit', 9: 'Soukpaserd', 10: 'Nhi', 11: 'Kha', 12: 'Huy', 13: 'Duc', 14: 'Sinh'}
        print names
        print "Done loading name, prepare load model..."
        load_trained_model(trained_data_path)
        print "Done loading model"
    elif opt == 't':
        print 'Training data....'
        train_recognizer(path, trained_data_path)

    while True:
        print "Please choose to use webcam or use test data: (w/t)"
        opt = raw_input()

        if opt == 'w':
            try:
                recognize_face_from_webcam(path)
            except Exception as e:
                print "Error when use webcam: {0}".format(e)
        else:
            recognize_from_test_data(img_test_path)
