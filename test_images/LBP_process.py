from skimage import feature
from imutils import paths
import cv2

cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

images_path = 'testLight'
radius = 1
n_points = 8
METHOD = 'uniform'

def detect_faces(gray_image):
    # this also detect object not face
    faces = faceCascade.detectMultiScale(
        gray_image,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags =  cv2.CASCADE_SCALE_IMAGE
    )
    result = []
    for (x, y, w, h) in faces:
        has_eye = False
        roi_gray = gray_image[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)

        if len(eyes):
            has_eye = True

        if has_eye:
            element = [x, y, w, h]
            result.append(element)
    return result


for imagePath in paths.list_images(images_path):
	# load the image, convert it to grayscale, convert it to LBH -> display
	print imagePath
	image = cv2.imread(imagePath, cv2.IMREAD_GRAYSCALE)
	faces = detect_faces(image)
	for (x, y, w, h) in faces:
		lbp_image = feature.local_binary_pattern(image[y: y + h, x: x + w], radius, n_points, METHOD)
		cv2.imshow("Image", lbp_image)
		cv2.waitKey(0)