## Bee-Tech face recognizer

### Prerequisites
- OpenCV 2.x installed
- Open CV python binding installed
- Computer has webcam or build-in camera
- numpy installed

### How to run
```
$ python face_recognizer.py
```
Info in the image is `name` and `confidence`
ex: `Hoang_37.25431356` with name: Hoang and confidence: 37.25431356
The more the value of confidence variable is, the less the recognizer has confidence in the recognition. A confidence value of 0.0 is a perfect recognition.

### Add image for trainning
Create a subdirectory inside `images` and named it with name of a person. Then add all images of that person to that directoty. Code will automaticlly load to train the recognizer

### Troubleshooting

If you get error: `HIGHGUI ERROR: V4L: index 0 is not correct!`. Go change `webcam = cv2.VideoCapture(1)` to `webcam = cv2.VideoCapture(0)`.
Because it depend on your build-in camera or external webcam