import cv2, os
import numpy as np

DEBUG = False

# For face detection we will use the Haar Cascade provided by OpenCV.
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
# Todo: refactor
names = {}

# For face recognition we will the the LBPH Face Recognizer 
recognizer = cv2.createLBPHFaceRecognizer()

def load_images_and_labels(path):
    # each subdirectory in path contain a set of images for that person
    # name of subdirectory indicate name of person inside
    subdirectories = []
    # labels contain integer number of each person
    images = []
    labels = []
    # names contain pair of number and name
    global names

    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path, name)):
            subdirectories.append(name)

    current_label = 0
    for directory in subdirectories:
        current_dir = os.path.join(path, directory)
        image_paths = [os.path.join(current_dir, f) for f in os.listdir(current_dir)]

        for image_path in image_paths:
            # Read the image and convert to grayscale
            gray_img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
            faces = faceCascade.detectMultiScale(gray_img)
            # If face is detected, append the face to images and the label to labels
            for (x, y, w, h) in faces:
                images.append(gray_img[y: y + h, x: x + w])
                labels.append(current_label)
                names[current_label] = directory
                if DEBUG:
                    cv2.imshow("Adding faces to traning set...", gray_img[y: y + h, x: x + w])
                    cv2.waitKey(1000)

        current_label = current_label + 1
    print "Label and names:", names
    return images, labels, names

def train_recognizer(path):
    images, labels, names = load_images_and_labels(path)
    recognizer.train(images, np.array(labels))
    # recognizer.setLabelsInfo(names)

def recognize_face_from_webcam(path):
    train_recognizer(path)
    webcam = cv2.VideoCapture(0)
    threshold = 40
    while True:
        return_value, frame = webcam.read()
        # turn the image captured to gray
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(gray_frame)
        for (x, y, w, h) in faces:
            # confidence = 0 is 100% accurate
            label_predicted, confidence = recognizer.predict(gray_frame[y: y + h, x: x + w])
            percent = round(100 - confidence)
            if percent < threshold:
                text = "Unknow"
            else:
                text = names[label_predicted] + "_" + str(percent) + " %"
            # name = recognizer.getLabelInfo(label_predicted)
            # draw a rectangle around face and label+confidence
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            cv2.putText(frame, text, (x, y - 10), 4, 1.0, (0, 255, 0), 2)
            cv2.imshow("Video", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    webcam.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    # ToDo: refactor
    path = "./images"
    recognize_face_from_webcam(path)

