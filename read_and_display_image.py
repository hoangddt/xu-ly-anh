import cv2

## Conslusion: gif format is not supported by default
## jpg and png are supported

jpg_image = './images/hoang.jpg'
png_image = './images/demo.png'
gif_image = './images/subject01.gif'
## cv2.imread(filename, flags=cv2.IMREAD_COLOR)
# flag can be: 
#  cv2.IMREAD_COLOR: : Loads a color image. Any transparency of image will be neglected. It is the default flag.
#  cv2.IMREAD_GRAYSCALE: Loads image in grayscale mode
#  cv2.IMREAD_UNCHANGED: Loads image as such including alpha channel

img = cv2.imread(gif_image)
if img is None:
	raise Exception("Can not load this image, extension is not support")
else:
	print "Load successed!"

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

cv2.imshow("Image", gray)
cv2.waitKey(0)
cv2.destroyAllWindows()